"""Environment Configurations."""
import myenv
import typing as typ


class App(myenv.BaseEnv):
    """General application related configuration."""

    env         : str
    service_name: str = "${MODULE_NAME}"
    log_level   : str = "DEBUG"


class Database(myenv.BaseEnv):
    """Database related configuration."""

    _environ_prefix = "DATABASE_"

    vendor         : str = "mysql"
    name           : str = "dbname"
    host           : str = "127.0.0.1"
    port           : int = 3306
    user           : str = "dbuser"
    password       : str

class HTTP(myenv.BaseEnv):
    """Configuration used by service_sdk.http."""

    _environ_prefix = "HTTP_"

    server : str = "gunicorn"
    addr   : str = "0.0.0.0"
    port   : int = 5000
    workers: int = 4
